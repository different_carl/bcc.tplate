# BCC Landing Page Template #

Contributors: differentstory

Requires at least: jQuery 1.7.x

Version: 1.0


Tested:

IE9+ | Chrome 63 to current | Firefox 57 to current | Safari 9.1 to current

IOS 9 to current | Android 4.4 to current

# Description #

BCC Landing Page Template for individual Chambers. Colours and Font can be changed, modules can be edited/removed where required.

# Installation #

1. Download repository (https://bitbucket.org/different_carl/bcc.tplate/overview)
2. The folder app/ is only required for deployment 
3. Those that have and use a preprocessor https://htmlmag.com/article/an-introduction-to-css-preprocessors-sass-less-stylus - SASS files are supplied for convenience. Otherwise the CSS files will need to be edited directly
4. Chamber developer to make changes to colour, font, modules, remove inline comments etc where applicable
5. Chamber developer to integrate with individual Chamber website
6. It is suggested that page template is integrated with a CMS for automation of advice and other content, but can be used as static file if required


# Bundle #


BCC Landing Page Template bundles the following third-party resources:

All high res images are included in src/original-imagery/

jQuery 1.12.4 which should be removed if already included on site

Source: https://code.jquery.com/


jquery.matchHeight.js, Copyright 2014 Liam Brummitt

License: MIT

Source: https://github.com/liabru/jquery-match-height


Bootstrap v3.3.7

Copyright 2011-2016 Twitter, Inc.

License: MIT

Source: https://getbootstrap.com/docs/3.3/

# Changelog #

= 1.0 =

* Released: March 23, 2018

Initial release
